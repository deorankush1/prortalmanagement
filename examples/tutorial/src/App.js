import React from 'react';
import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';
import { Admin, Resource, ListGuesser } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import { PostList, PostEdit, PostCreate, PostShow } from './posts';
import { UserList } from './users';
import Dashboard from './Dashboard/Dashboard';
import Account from './Account/Account';
import Products from './Products/Product';
import Explore from './Explore/Explore';
import Scheme from './Scheme/Scheme';
import Shipments from './Shipments/Shipment';
import Billing from './Billing/Billing';
import Order from './Orders/Order';

import authProvider from './authProvider';

const App = () => (
    <Admin
        dataProvider={jsonServerProvider(
            'https://jsonplaceholder.typicode.com'
        )}
        authProvider={authProvider}
        dashboard={Dashboard}
    >
        {/* <Resource
            name="posts"
            icon={PostIcon}
            list={PostList}
            edit={PostEdit}
            create={PostCreate}
            show={PostShow}
        />
        <Resource name="users" icon={UserIcon} list={UserList} />
        <Resource name="comments" list={ListGuesser} /> */}
        <Resource name="Orders" list={Order} />

        <Resource name="Account" icon={PostIcon} list={Account} />
        <Resource name="Products" list={Products} />
        <Resource name="Explore" list={Explore} />
        <Resource name="Scheme" list={Scheme} />
        <Resource name="Shipments" list={Shipments} />
        <Resource name="Billing" list={Billing} />
    </Admin>
);
export default App;
